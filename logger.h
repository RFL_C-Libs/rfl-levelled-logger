/* A levelled logger with colourized output.
NOT CURRENTLY THREAD-SAFE.

By R Landau
2023 */
#ifndef _LOGGER_H_INCLUDED_
#define _LOGGER_H_INCLUDED_

// Major version changes indicate potential breaking changes to the interface.
#define RFLLOGGER_VERSION "1.5"

#include <inttypes.h>

// debug types
enum level
{
  LOGLEVELDEBUG,
  LOGLEVELINFO,
  LOGLEVELWARN,
  LOGLEVELERR,
  LOGLEVELFATAL
};

// Exported type
// #######################################################################################################

typedef struct LoggerObj* Logger;

// Constructors
// ########################################################################################################

/* Returns a new logger set to only print fatal errors.
Color is disabled for file output.
@params - char* output_path: Defaults output_path to stdout.
@returns a new logger or NULL on failure */
Logger
NewProdLogger(char* output_path);

/*Returns a new logger set to print all messages.
@params - char* output_path: Defaults output_path to stdout.
@returns a new logger or NULL on failure */
Logger
NewDebugLogger(char* output_path);

/* Returns a new logger set to the given level.
@params - char* output_path: Defaults output_path to stdout.
@returns a new logger or NULL on failure */
Logger
NewLoggerWithLevel(char* output_path, enum level lev);

// Destructors
// #########################################################################################################
void
FreeLogger(Logger l);
// Logger Configurations
void
SetLevel(Logger l, enum level level);

// Print Functions
void
Debug(Logger l, char* format, ...);
void
Info(Logger l, char* format, ...);
void
Warn(Logger l, char* format, ...);
void
Err(Logger l, char* format, ...);
void
Fatal(Logger l, char* format, ...);

#endif