#include "logger.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "color.h"

#define DEBUG_PREFIX "DBG: "
#define INFO_PREFIX "INF: "
#define WARN_PREFIX "WRN: "
#define ERR_PREFIX "ERR: "
#define FATAL_PREFIX "FTL: "

typedef struct LoggerObj
{
  enum level cur_level; // Default:
  uint8_t colorized;    // Default: true. 0 is false. Anything else is true //
                        // TODO implement disable-color
  FILE* output;         // Default: stdout
} LoggerObj;

// CONSTRUCTORS ----------------------------------------------------------------

// internal constructor
// opens streams in append mode
Logger
initLogger(enum level lev, char* output_path)
{
  Logger l;
  l = malloc(sizeof(LoggerObj));
  // set defaults
  l->cur_level = lev;
  l->colorized = 1;
  l->output = stdout;

  if (output_path != NULL) {
    l->output = fopen(output_path, "a");
    if (l->output == NULL) {
      perror("Failed to initialize logger");
      l = NULL;
    }
  }

  // disable colorization for file output
  if (l->output != stdout && l->output != stderr)
    l->colorized = 0;

  Info(l, "logger initialized");

  return l;
}

Logger
NewProdLogger(char* output_path)
{
  return initLogger(LOGLEVELFATAL, output_path);
}

Logger
NewDebugLogger(char* output_path)
{
  return initLogger(LOGLEVELDEBUG, output_path);
}

Logger
NewLoggerWithLevel(char* output_path, enum level lev)
{
  return initLogger(lev, output_path);
}

// DESTRUCTOR ------------------------------------------------------------------

/* Flushes, closes, and frees the logger. */
void
FreeLogger(Logger l)
{
  Debug(l, "Flushing...");
  if (fflush(l->output) == EOF)
    perror("Failed to flush logger output stream");
  if (fclose(l->output) == EOF)
    perror("Failed to close logger output stream");
  free(l);
  l = NULL;
}

// METHODS ---------------------------------------------------------------------

/* level enum stringification */
char*
strlevel(enum level l)
{
  switch (l) {
    case LOGLEVELDEBUG: {
      return "debug";
    }
    case LOGLEVELINFO:
      return "info";
    case LOGLEVELWARN:
      return "warn";
    case LOGLEVELERR:
      return "err";
    case LOGLEVELFATAL:
      return "fatal";
    default:
      return NULL;
  }
}

// configuring the logger ------------------------------------------------------
void
SetLevel(Logger l, enum level level)
{
  l->cur_level = level;
}

// printing from the logger ----------------------------------------------------
void
print(Logger log, char* color, char* prefix, char* format, va_list args)
{
  if (log->colorized)
    fputs(color, log->output);
  fputs(prefix, log->output);
  if (log->colorized)
    fputs(SGR_COLOR_RESET, log->output);
  vfprintf(log->output, format, args);
  fputs("\n", log->output);
}

void
Debug(Logger l, char* format, ...)
{
  if (l == NULL)
    return;
  if (LOGLEVELDEBUG >= l->cur_level) {
    va_list args;
    va_start(args, format);
    print(l, SGR_COLOR_MAGENTA, DEBUG_PREFIX, format, args);
    va_end(args);
  }
}

void
Info(Logger l, char* format, ...)
{
  if (l == NULL)
    return;
  if (LOGLEVELINFO >= l->cur_level) {
    va_list args;
    va_start(args, format);
    print(l, SGR_COLOR_CYAN, INFO_PREFIX, format, args);
    va_end(args);
  }
}

void
Warn(Logger l, char* format, ...)
{
  if (l == NULL)
    return;
  if (LOGLEVELWARN >= l->cur_level) {
    va_list args;
    va_start(args, format);
    print(l, SGR_COLOR_YELLOW, WARN_PREFIX, format, args);
    va_end(args);
  }
}

void
Err(Logger l, char* format, ...)
{
  if (l == NULL)
    return;
  if (LOGLEVELERR >= l->cur_level) {
    va_list args;
    va_start(args, format);
    print(l, SGR_COLOR_RED, ERR_PREFIX, format, args);
    va_end(args);
  }
}

void
Fatal(Logger l, char* format, ...)
{
  if (l == NULL)
    return;
  if (LOGLEVELFATAL >= l->cur_level) {
    va_list args;
    va_start(args, format);
    print(l, SGR_COLOR_BOLD_RED, FATAL_PREFIX, format, args);
    va_end(args);
  }
}
