/* defines color parameters for use by the logger.
https://en.wikipedia.org/wiki/ANSI_escape_code#Colors */

// 4-bit colors
#define SGR_COLOR_MAGENTA "\x1b[35m"
#define SGR_COLOR_CYAN "\x1b[36m"
#define SGR_COLOR_YELLOW "\x1b[33m"
#define SGR_COLOR_RED "\x1b[31m"
#define SGR_COLOR_BOLD_RED "\x1b[1;31m"

#define SGR_COLOR_RESET "\x1b[0m"