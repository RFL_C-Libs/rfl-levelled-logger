// An incredibly barebones way to test the logger quickly.

#include <stdio.h>
#include "logger.h"

void printEachLevel(Logger l){
    Debug(l, "debug msg");
    Info(l, "info msg");
    Warn(l, "warn msg");
    Err(l, "error msg");
    Fatal(l, "fatal msg");
}

// MAIN ------------------------------------------------------------------------
int main(int argc, char* argv[]){
    Logger l_stdout = NewLoggerWithLevel((NULL), LOGLEVELINFO);
    printf("test - default logger level\n");
    printEachLevel(l_stdout);
    FreeLogger(l_stdout);
    
    Logger l_file = NewProdLogger("test.log");

    printf("test - default logger level\n");
    printEachLevel(l_file);
    printf("test - debug logger level\n");
    SetLevel(l_file, LOGLEVELDEBUG);
    printEachLevel(l_file);
    printf("test - fatal logger level\n");
    SetLevel(l_file, LOGLEVELFATAL);
    printEachLevel(l_file);
    
    FreeLogger(l_file);
    return 0;
}