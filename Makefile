# ------------------------------------------------------------------------------
#
# ------------------------------------------------------------------------------

HEADERS 		= color.h logger.h
TEST_BINARY 	= test_logger
LIB_SOURCES 	= logger.c
OBJECTS 		= logger.o

# generates an optimized .o file we can use to bring this code into other projects
lib_prod : logger.c logger.h
	clang -c -std=c99 -O2 logger.c

# note the use of gdwarf-4 instead of -g; this is to ensure clang's compat with valgrind
test : logger.o
	clang -o $(TEST_BINARY) -std=c99 -Wall -gdwarf-4 -O0 -lefence test.c logger.o

logger.o : logger.c logger.h
	clang -c -std=c99 -O0 -Wall -gdwarf-4 logger.c

format : logger.c $(HEADERS)
	clang-format -i -style=mozilla logger.c $(HEADERS)

clean :
	rm -f $(TEST_BINARY) $(OBJECTS) *.log

memtest : test
	valgrind --leak-check=full --track-origins=yes -s ./test_logger 