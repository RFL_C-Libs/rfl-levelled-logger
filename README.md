# RFL Levelled Logger

A small, colourized, levelled logger utility.

# Features 

- [x] 5 log levels: DEBUG, INFO, WARN, ERROR, FATAL
- [x] printf formatting (via variadic passthrough)
- [x] colourized output to terminals (via ANSI escape sequences)
- [x] c99
- [x] type portability (via inttypes.h)
- [x] pure C
- [ ] thread safety

# Install

To start using this library, you will need a logger.o object file, the logger.h file, and the color.h file.

**NOTE**: I use clang and therefore so does the Makefile. You could use gcc all the same.

1. Clone the repo
2. `cd rfl-levelled-logger`
3. `make`
    - If Make is not availabe on your machine, you can compile the library manually: `clang -c -std=c99 -O2 logger.c`
4. Copy logger.h, logger.o into your source directory and `include "logger.h"` in your code.
5. When you are ready to compile, make sure you include logger.o.
    - ex: `clang -o my_output_binary -std=c99 -Wall -gdwarf-4 -O0 test.c logger.o`

# Usage

Generally, you will just call `NewLoggerWithLevel` and pass in an enumerated level (LOGLEVELDEBUG, LOGLEVELINFO, LOGLEVELWARN, LOGLEVELERR, LOGLEVELFATAL).

```c
#include <stdio.h>
#include "logger.h"

int main(int argc, char* argv[]){
    Logger l = NewLoggerWithLevel((NULL), LOGLEVELWARN);

    Debug(l, "debug msg"); // this will not be printed
    Info(l, "info msg"); // neither will this
    Warn(l, "warn msg");
    Err(l, "error msg");
    Fatal(l, "fatal msg");

    FreeLogger(l);
}
```

## Notes

- Fatal does *not* call exit().
- Passing `NULL` to any of the constructors defaults the logger to `stdout`.